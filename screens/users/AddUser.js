import React, { useState } from 'react';
import { Button, ScrollView, TextInput, View, StyleSheet } from 'react-native';
import firebase from './../../database/Config.js';

const AddUser = (props) => {
    // Object state
    const [profile, setProfile] = useState({
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
    });

    // Change inputGroup
    const handleChangeText = (key, value) => {
        setProfile({ ...profile, [key]: value });
    };

    // Object destructuring of the profile
    const { name, email, password, confirmPassword } = profile;

    // User to add
    const userCloud = {
        name: name,
        email: email,
        password: password,
        confirmPassword: confirmPassword,
    };

    // Add new user
    const addNewUser = async () => {
        if (name === '') {
            alert('Please provide a name :(');
        } else {
            await firebase.firestoreCloud.collection('users').add(userCloud);
            alert('Good job :D, the user has been added.');
            props.navigation.navigate('PhotoShare');
        }
    };

    return (
        // Form User
        <ScrollView style={styles.container}>
            <View style={styles.inputGroup}>
                <TextInput
                    placeholder='Name User'
                    onChangeText={(value) => handleChangeText('name', value)}
                />
            </View>
            <View style={styles.inputGroup}>
                <TextInput
                    placeholder='Email User'
                    onChangeText={(value) => handleChangeText('email', value)}
                />
            </View>
            <View style={styles.inputGroup}>
                <TextInput
                    placeholder='Password User'
                    onChangeText={(value) =>
                        handleChangeText('password', value)
                    }
                />
            </View>
            <View style={styles.inputGroup}>
                <TextInput
                    placeholder='Confirm Password User'
                    onChangeText={(value) =>
                        handleChangeText('confirmPassword', value)
                    }
                />
            </View>
            <View style={styles.inputGroup}>
                <Button title='Add User' onPress={addNewUser} />
            </View>
        </ScrollView>
    );
};

export default AddUser;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 35,
    },

    inputGroup: {
        flex: 1,
        padding: 0,
        // margin: 10,
        flexDirection: 'row',
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#E1E1E1',
    },
});
