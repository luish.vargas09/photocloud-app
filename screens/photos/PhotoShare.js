import React, { useState } from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Platform,
} from 'react-native';
import logo from './../../assets/images/rocket-g9971c3756_1280.png';
import * as ImagePicker from 'expo-image-picker';
import * as Sharing from 'expo-sharing';
import * as ImageManipulator from 'expo-image-manipulator';

export default function App() {
    const [selectedImage, setSelectedImage] = useState(null);

    // PICK UP THE IMAGE
    let openImagePickerAsync = async () => {
        // Permissions
        let pickerResult = await ImagePicker.launchImageLibraryAsync();
        // console.log(pickerResult);
        if (pickerResult.cancelled === true) return;

        // Set the selected image on the screen
        if (Platform.OS === 'web') {
            const remoteUri = await uploadToAnonymousFilesAsync(
                pickerResult.uri
            );
            // console.log(remoteUri);
            setSelectedImage({
                localUri: pickerResult.uri,
                remoteUri,
            });
        } else {
            setSelectedImage({ localUri: pickerResult.uri });
        }
    };

    // On the web does not support
    let openShareDialogAsync = async () => {
        if (Platform.OS == 'web') {
            alert('Uh oh sorry... sharing is not avaible on you platform');
            return;
        }

        // SHARING IMAGE
        const imageTmp = await ImageManipulator.manipulateAsync(
            selectedImage.localUri
        );
        await Sharing.shareAsync(imageTmp.uri);
    };

    // IMAGE SELECTED READY FOR SHARING
    if (selectedImage !== null) {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={openImagePickerAsync}>
                    <Image
                        // source={{ uri: 'https://picsum.photos/200/300' }}
                        // source={image}
                        source={{
                            uri:
                                selectedImage !== null
                                    ? selectedImage.localUri
                                    : { logo },
                        }}
                        style={styles.thumbail}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={openShareDialogAsync}
                    style={styles.button}
                >
                    <Text style={styles.buttonText}>Share this photo</Text>
                </TouchableOpacity>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>PhotoCloud</Text>
            <Image source={logo} style={{ width: 305, height: 159 }} />
            {/* <Image source={{ uri: uri }} style={logo} /> */}
            <Text style={styles.instructions}>
                {' '}
                To share a photo from your phone with a friend, just press the
                button below!
            </Text>

            {/* Button */}
            <TouchableOpacity
                onPress={openImagePickerAsync}
                style={styles.button}
            >
                <Text style={styles.buttonText}>Pick a photo</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#161616',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 305,
        height: 159,
        marginBottom: 10,
    },
    instructions: {
        color: '#888',
        fontSize: 18,
        marginHorizontal: 15,
        margin: 25,
        textAlign: 'center',
    },
    button: {
        backgroundColor: 'blue',
        padding: 20,
        borderRadius: 5,
        margin: 15,
    },
    buttonText: {
        fontSize: 20,
        color: '#fff',
    },
    thumbail: {
        width: 300,
        height: 300,
        resizeMode: 'contain',
    },
    title: {
        fontSize: 30,
        color: 'deepskyblue',
        textAlign: 'center',
        marginBottom: 20,
    },
});
