import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import PhotoShare from './screens/photos/PhotoShare';
import AddUser from './screens/users/AddUser';
import ProfileUser from './screens/users/ProfileUser';
import UsersList from './screens/users/UsersList';

const Stack = createNativeStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='AddUser' component={AddUser} />
                <Stack.Screen name='PhotoShare' component={PhotoShare} />
                <Stack.Screen name='ProfileUser' component={ProfileUser} />
                <Stack.Screen name='UsersList' component={UsersList} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;
