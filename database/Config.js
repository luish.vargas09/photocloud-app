import firebase from 'firebase';
import 'firebase/firestore';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: 'AIzaSyDY_PfDCT05bmx18ZklaJ1v_5luF5EHNF4',
    authDomain: 'photocloud-app-e24f8.firebaseapp.com',
    projectId: 'photocloud-app-e24f8',
    storageBucket: 'photocloud-app-e24f8.appspot.com',
    messagingSenderId: '257233051909',
    appId: '1:257233051909:web:82440c7e1ed6d62de9576c',
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const firestoreCloud = firebase.firestore();

export default {
    firebase,
    firestoreCloud,
};
